﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace cStoreMVC.Data.EF
{
    class GenreMetadata
    {
        //public int GenreID { get; set; }
        [Required(ErrorMessage ="genre name required")]
        [Display(Name ="Genre Name")]
        public string GenreName { get; set; }

    }
}
