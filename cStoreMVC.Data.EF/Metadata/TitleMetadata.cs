﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cStoreMVC.Data.EF
{

    [MetadataType(typeof(TitleMetadata))]

    public partial class title { }


    class TitleMetadata
    {
        public int TitleID { get; set; }
        [StringLength(17, MinimumLength =13, ErrorMessage ="isbn numbers are 10 digits with 3 dashes or 13didgits with 4 dashes")]
        public string ISBN { get; set; }

        [Required(ErrorMessage = "ReQuIrrED")]
        [Display(Name ="Title")]
        public string BookTitle { get; set; }

        [UIHint("Multiline")]//builds relatged fields as text areas
        public string Description { get; set; }
        public Nullable<int> GenreID { get; set; }

        [DisplayFormat(DataFormatString ="{0:c}")]
        [Range(.01,1000, ErrorMessage ="price must be between.01-1000")]
        public Nullable<decimal> Price { get; set; }

        [Display(Name = "Units Sold")]
        public Nullable<int> UnitsSold { get; set; }
        [Display(Name = "publish date")]

        public Nullable<System.DateTime> PublishDate { get; set; }
        [Display(Name = "Publishers ID")]

        public int PublisherID { get; set; }
        [Display(Name = "Image")]

        public string BookImage { get; set; }
        [Display(Name = "site Feature?")]

        public bool IsSiteFeature { get; set; }
        [Display(Name = "Genre Feature?")]

        public bool IsGenreFeature { get; set; }
        [Display(Name = "Ebook?")]

        public bool IsEBook { get; set; }

    }
}
