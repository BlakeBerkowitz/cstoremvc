﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace cStoreMVC.Data.EF
{
    public class AuthorMetadata
    {

        //public int AuthorID { get; set; }
        [Required(ErrorMessage ="name required")]
        [StringLength(15,ErrorMessage ="first name must be 15 characters or less")]
        [Display(Name ="first name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "name required")]
        [StringLength(15, ErrorMessage = "last name must be 15 characters or less")]
        [Display(Name = "last name")]
        public string LastName { get; set; }
        [StringLength(50,ErrorMessage ="city name limited to 50 characters")]
        [DisplayFormat(NullDisplayText ="[-N/A-]")]
        public string City { get; set; }
        [StringLength(2,ErrorMessage ="use 2 letter abbreviation for state")]
        [DisplayFormat(NullDisplayText = "[-N/A-]")]

        public string State { get; set; }
        [StringLength(6,ErrorMessage ="zip code is limited to 6 characters")]
        [DisplayFormat(NullDisplayText = "[-N/A-]")]
        [Display(Name ="zip code")]

        public string ZipCode { get; set; }
        [StringLength(30,ErrorMessage ="country is limited to 30 characters")]
        [DisplayFormat(NullDisplayText = "[-N/A-]")]


        public string Country { get; set; }


    }

    [MetadataType(typeof(AuthorMetadata))]
    public partial class Author {
        //attach our custom prop to the partial class
        [Display(Name ="Name")]
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
        //create location city, state zip country

        [Display(Name ="location")]
        public string Location
        {
            //get { return City + ", " + State + @"  " + ZipCode + " " + Country; }
            get { return $"{City} , {State}  {ZipCode} {Country}"; }
        }

    }
}
