﻿using cStore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cStoreMVC.Data.linq
{
    public class LinqDAL
    {
        cStoreLinqtoSQLDataContext db = new cStoreLinqtoSQLDataContext();
        public List<TitleGenreViewModel> GetFeaturedBooks()
        {
            var siteFeatures = from t in db.LinqTitles
                               join g in db.LinqGenres on t.GenreID equals g.GenreID
                               where t.IsSiteFeature == true
                               orderby t.BookTitle
                               select new
                               {
                                   t.TitleID,
                                   t.ISBN,
                                   t.BookTitle,
                                   t.BookImage,
                                   t.Price,
                                   t.Description,
                                   g.GenreName
                               };
            var siteFeaturesList = new List<TitleGenreViewModel>();

            foreach (var item in siteFeatures)
            {
                siteFeaturesList.Add(
                    new TitleGenreViewModel()
                    {
                        TitleID = item.TitleID,
                        ISBN = item.ISBN,
                        BookImage = item.BookImage,
                        BookTitle = item.BookTitle,
                        Price = item.Price,
                        Description = item.Description,
                        GenreName = item.GenreName


                    });
            }//end foreach
            return siteFeaturesList;
                               
        }

        public List<LinqMagazine> GetMagazines()
        {
            List<LinqMagazine> magazines = db.LinqMagazines.OrderBy(m => m.MagazineTitle).ToList();
            return (magazines);
        }

        public List<TitleGenreViewModel> GetTitles()
        {
            var titles = from t in db.LinqTitles
                               join g in db.LinqGenres on t.GenreID equals g.GenreID
                               where t.Price < 20
                               orderby t.GenreID, t.BookTitle
                               select new
                               {
                                   t.TitleID,
                                   t.ISBN,
                                   t.BookTitle,
                                   t.BookImage,
                                   t.Price,
                                   t.Description,
                                   g.GenreName
                               };
            var titlesList = new List<TitleGenreViewModel>();

            foreach (var item in titles)
            {
                titlesList.Add(
                    new TitleGenreViewModel()
                    {
                        TitleID = item.TitleID,
                        ISBN = item.ISBN,
                        BookImage = item.BookImage,
                        BookTitle = item.BookTitle,
                        Price = item.Price,
                        Description = item.Description,
                        GenreName = item.GenreName


                    });
            }//end foreach
            return titlesList;

        }



    }
}
