﻿using cStoreMVC.Data.linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cStoreMVC.UI.Controllers
{
    public class LinqController : Controller


        
    {
        LinqDAL linq = new LinqDAL();
        // GET: Linq
        public ActionResult GetFeaturedBooks()
        {
            return View(linq.GetFeaturedBooks());
        }


        public ActionResult Magazines()
        {
            List<LinqMagazine> magazines = linq.GetMagazines();

            ViewBag.SportMagazines = magazines.Where(m => m.Category == "Sports");
            ViewBag.KidsMagazinesRanked = magazines.Where(m => m.Category == "Kids").OrderBy(m => m.Circulation);
            return View(magazines);
        }

        public ActionResult Titles()
        {
            return View(linq.GetTitles());
        }
    }
}