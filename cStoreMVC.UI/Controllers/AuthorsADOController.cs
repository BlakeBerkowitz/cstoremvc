﻿using cStore.Domain.Models;
using cStoreMVC.Data.ADO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cStoreMVC.UI.Controllers
{
    public class AuthorsADOController : Controller
    {

        AuthorsDAL authors = new AuthorsDAL();




        // GET: AuthorsADO
        public ActionResult Index()
        {
            ViewBag.firstnames = authors.GetFirstnames();
            return View();
        }

        public ActionResult GetAuthors()
        {
            return View(authors.GetAuthors());
        }

        public ActionResult CreateAuthor()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAuthor(AuthorModel author)
        {
            if (ModelState.IsValid)
            {


                authors.CreateAuthor(author);
                return RedirectToAction("GetAuthors");
            }
            return View(author);

        }

        public ActionResult UpdateAuthor(int id)
        {
            return View(authors.GetAuthor(id));
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateAuthor(AuthorModel author)
        {
            if (ModelState.IsValid)
            {
                authors.Updateauthor(author);
                return RedirectToAction("GetAuthors");

            }
            return View(author);
        }

        public ActionResult DeleteAuthor(int id)
        {
            authors.DeleteAuthor(id);
            return RedirectToAction("GetAuthors");
        }

    }
}