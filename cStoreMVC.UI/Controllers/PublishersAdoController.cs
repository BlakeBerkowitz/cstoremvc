﻿using System;
using cStoreMVC.Data.ADO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cStore.Domain.Models;

namespace cStoreMVC.UI.Controllers
{
    public class PublishersAdoController : Controller
    {
        PublishersDAL publisher = new PublishersDAL();
        // GET: PublishersAdo
        public ActionResult Index()
        {
            ViewBag.publisherName = publisher.GetPublishersName();

            return View();
        }
        public ActionResult GetPublishers()
        {
            return View(publisher.GetPublishers());
        }
    }
}