﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using cStoreMVC.Data.EF;
using cStore.Domain.repositories;
using cStore.Domain;
using LinqKit;

namespace cStoreMVC.UI.Controllers
{
    public class TitlesController : Controller
    {
        //private cStoreEntities db = new cStoreEntities();
        //TitleRepository repo = new TitleRepository();
        UnitOfWork uow = new UnitOfWork();
        //create repository instancese for DropDownList Population
        //PublisherRepository repoP = new PublisherRepository();

        //GenreRepository repoG = new GenreRepository();

            //Search implemented in IGenericRepo and Generic repo
            public ActionResult Search()
        {
            return View();
        }

        //process the search results
        //parameters define the fields searchable and the ones marked join give us the ands/ors in the where
        public ActionResult SearchResults(string bookTitle, string joinPubDate, string startPubDate,string endPubDate, string joinDescription,
            string description, string joinPrice, decimal? price)
        {
            //create var that builds our linq expression
            var predicate = PredicateBuilder.New<Title>();

            //evaluate and handle the values passed in thru the Action.
            if (!string.IsNullOrEmpty(bookTitle))
            {
                predicate = predicate.And(e => e.BookTitle.Contains(bookTitle));
            }

            //these are created so that if tryparse fails we dont end up with object reference not set to an instance
            DateTime startDateRange = new DateTime();
            DateTime endDateRange = new DateTime();

            //attempt to parse the date time out is what to use if failure to parse occurs
            if (DateTime.TryParse(startPubDate, out startDateRange) &&(DateTime.TryParse(endPubDate, out endDateRange)))
            {
                //by the value of join add and/or to our search query
                if (joinPubDate == "and")
                {
                    predicate = predicate.And(e => e.PublishDate.Value >= startDateRange && e.PublishDate.Value <= endDateRange);

                }
                else
                {
                    predicate = predicate.Or(e => e.PublishDate.Value >= startDateRange && e.PublishDate.Value <= endDateRange);
                }
            }

            if (!string.IsNullOrEmpty(joinDescription))
            {
                if (joinDescription =="and")
                {
                    predicate = predicate.And(e => e.Description.Contains(description));
                }
                else
                {
                    predicate = predicate.Or(e => e.Description.Contains(description));

                }
            }
            if (price !=null)
            {
                if (joinPrice == "and")
                {
                    predicate = predicate.And(e => e.Price.Value <= price);

                }
                else
                {
                    predicate = predicate.Or(e => e.Price.Value >= price);
                }
            }

            //return the results after compiling the query and executing via the repository
            return View(uow.TitleRepository.Search(predicate).OrderBy(m => m.BookTitle));

        }



        // GET: Titles
        public ActionResult Index()
        {
            //var titles = db.Titles.Include(t => t.Genre).Include(t => t.Publisher);
            //this is why we have the for each in the generic repo
            return View(uow.TitleRepository.Get("Genre, Publisher"));
            //this result is the same as the linq statement above witht he includes

        }

        // GET: Titles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Title title = uow.TitleRepository.Find(id);
            if (title == null)
            {
                return HttpNotFound();
            }
            return View(title);
        }

        // GET: Titles/Create
        public ActionResult Create()
        {

            //create instaces of the genre repository and the publishers repository so that thaey can be used as a data source for the dropdown lists
            //see the top of the class for those declarations.

            //this is the id called by the view to populate the ddl
            ViewBag.GenreID = 
                //create a new ddl ([querythat returns the table represented]
                new SelectList(uow.GenreRepository.Get(),
                //[Value],[displaytext]
                "GenreID", "GenreName");
            ViewBag.PublisherID = new SelectList(uow.PublisherRepository.Get(), "PublisherID", "PublisherName");
            return View();
        }

        // POST: Titles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TitleID,ISBN,BookTitle,Description,GenreID,Price,UnitsSold,PublishDate,PublisherID,BookImage,IsSiteFeature,IsGenreFeature,IsEBook")] Title title)
        {
            if (ModelState.IsValid)
            {
                uow.TitleRepository.Add(title);
                uow.Save();
                    return RedirectToAction("Index");
            }

            ViewBag.GenreID = new SelectList(uow.GenreRepository.Get(), "GenreID", "GenreName", title.GenreID);
            ViewBag.PublisherID = new SelectList(uow.PublisherRepository.Get(), "PublisherID", "PublisherName", title.PublisherID);
            return View(title);
        }

        // GET: Titles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Title title = uow.TitleRepository.Find(id);
            if (title == null)
            {
                return HttpNotFound();
            }
            ViewBag.GenreID = new SelectList(uow.GenreRepository.Get(), "GenreID", "GenreName", title.GenreID);
            ViewBag.PublisherID = new SelectList(uow.PublisherRepository.Get(), "PublisherID", "PublisherName", title.PublisherID);
            return View(title);
        }

        // POST: Titles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TitleID,ISBN,BookTitle,Description,GenreID,Price,UnitsSold,PublishDate,PublisherID,BookImage,IsSiteFeature,IsGenreFeature,IsEBook")] Title title)
        {
            if (ModelState.IsValid)
            {
                uow.TitleRepository.Update(title);
                uow.Save();
                    return RedirectToAction("Index");
            }
            ViewBag.GenreID = new SelectList(uow.GenreRepository.Get(), "GenreID", "GenreName", title.GenreID);
            ViewBag.PublisherID = new SelectList(uow.PublisherRepository.Get(), "PublisherID", "PublisherName", title.PublisherID);
            return View(title);
        }

        // GET: Titles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Title title = uow.TitleRepository.Find(id);
            if (title == null)
            {
                return HttpNotFound();
            }
            return View(title);
        }

        // POST: Titles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Title title = uow.TitleRepository.Find(id);
            uow.TitleRepository.Remove(title);
            uow.Save();
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
