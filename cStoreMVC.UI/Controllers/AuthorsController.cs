﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using cStoreMVC.Data.EF;
using cStore.Domain.repositories;
using cStore.Domain;

namespace cStoreMVC.UI.Controllers
{

    /// <summary>
    /// implementation of the repository from the domain was used to cchieve single responsibility for the controller. 
    /// prior to this implemenation the controller was responsible for handling views marshalling data between the controller views
    /// and managing Data Access Functions.
    /// 
    /// </summary>
    /// 
   
    public class AuthorsController : Controller
    {

        //private cStoreEntities db = new cStoreEntities();
        //AuthorRepository repo = new AuthorRepository();
        UnitOfWork uow = new UnitOfWork();
        // GET: Authors
        public ActionResult Index()
        {
            //return View(db.Authors.ToList());
            return View(uow.AuthorRepository.Get());
        }

        // GET: Authors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Author author = db.Authors.Find(id);
            Author author = uow.AuthorRepository.Find(id);
            if (author == null)
            {
                return HttpNotFound();
            }
            return View(author);
        }

        // GET: Authors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Authors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AuthorID,FirstName,LastName,City,State,ZipCode,Country")] Author author)
        {
            if (ModelState.IsValid)
            {
                //db.Authors.Add(author);
                //db.SaveChanges();
                uow.AuthorRepository.Add(author);
                uow.Save();
                return RedirectToAction("Index");
            }

            return View(author);
        }

        // GET: Authors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Author author = db.Authors.Find(id);
            Author author = uow.AuthorRepository.Find(id);
            if (author == null)
            {
                return HttpNotFound();
            }
            return View(author);
        }

        // POST: Authors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AuthorID,FirstName,LastName,City,State,ZipCode,Country")] Author author)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(author).State = EntityState.Modified;
                //db.SaveChanges();
                uow.AuthorRepository.Update(author);
                uow.Save();
                return RedirectToAction("Index");
            }
            return View(author);
        }

        // GET: Authors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Author author = db.Authors.Find(id);
            Author author = uow.AuthorRepository.Find(id);
            if (author == null)
            {
                return HttpNotFound();
            }
            return View(author);
        }

        // POST: Authors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Author author = db.Authors.Find(id);
            //db.Authors.Remove(author);
            //db.SaveChanges();
            Author author = uow.AuthorRepository.Find(id);
            uow.AuthorRepository.Remove(author);
            uow.Save();
            return RedirectToAction("Index");
        }

        //commented out bc our repo handles all things database including the disposal of database objects and connections. the dispose
        //method is scaffolded for an ef controller expecting the controller to manage the connections when we break out the Data Access
        //potion we also care for the disposal. (see authorrepo (bottom of the class) in the domain layer)


        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        //db.Dispose();
               
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
