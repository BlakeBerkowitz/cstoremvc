﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cStoreMVC.Data.EF;
using System.Net;

namespace cStoreMVC.UI.Controllers
{
    public class MagazinesController : Controller
    {

        cStoreEntities db = new cStoreEntities();

        // GET: Magazines
        public ActionResult Index()
        {
            List<Magazine> magazines = db.Magazines.ToList();
            return View(magazines);

        }

        public ActionResult Details(int id)
        {
            return View(db.Magazines.Find(id));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include ="MagazineID, MagazineTitle, IssuesPerYear, PricePerYear, Category, Circulation, PublishRate")]Magazine magazine)
        {
            if (ModelState.IsValid)
            {
                db.Magazines.Add(magazine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(magazine);

        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Magazine magazine = db.Magazines.Find(id);
            if (magazine == null)
            {
                return HttpNotFound();

            }
            return View(magazine);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MagazineID, MagazineTitle, IssuesPerYear, PricePerYear, Category, Circulation, PublishRate")]Magazine magazine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(magazine).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(magazine);

        }

        public ActionResult Delete(int id)
        {
            Magazine magazine = db.Magazines.Find(id);
            db.Magazines.Remove(magazine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {

            if (disposing)
            {
                db.Dispose();
            }



            base.Dispose(disposing);
        }


    }
}