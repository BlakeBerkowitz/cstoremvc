﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cStoreMVC.UI.Models;
using System.Net.Mail;
using System.Net;

namespace cStoreMVC.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FAQ()
        {

            return View();
        }

        public ActionResult Contact()
        {

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {
            //create the body for the email
            string body = string.Format("Name: {0}<br/>Email: {1}<br/>Subject: {2}<br/>{3}", contact.Name, contact.Email, contact.Subject, contact.Message);

            //creat and configure the mail message
            MailMessage msg = new MailMessage("no-reply@blakeberkowitz.com", "Blake@kc-berkowitz.com");


            //configure mail message object
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.High;

            //create and configure the smtp client
            SmtpClient client = new SmtpClient("mail.blakeberkowitz.com");
            client.Credentials = new NetworkCredential("no-reply@blakeberkowitz.com", "P@ssw0rd");


            using (client)
            {
                try
                {
                    client.Send(msg);
                }
                catch
                {
                    ViewBag.ErrorMessage = "there was an error sending your email plz try again later.";
                    return View();
                }
            }


            //send the user to the contact confirmation viw and pass the object with it


            return View("ContactConfirmation", contact);
        }

        public ActionResult Cafe()
        {
            throw new System.Exception("cafe is clsoed");
        }

    }
}