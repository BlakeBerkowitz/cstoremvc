﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(cStoreMVC.UI.Startup))]
namespace cStoreMVC.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
