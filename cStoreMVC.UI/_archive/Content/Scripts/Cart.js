﻿var books = [
    {
        id: 1,
        title: "The Eye of Minds",
        author: "James Dashner",
        price: 9.99
    },
    {
        id: 2,
        title: "Harold and the Purple Crayon",
        author: "Crocket Johnson",
        price: 6.95
    },
    {
        id: 3,
        title: "Essential C# 6.0",
        author: "Mark Michaelis & Eric Lippert",
        price: 46.99
    },
    {
        id: 4,
        title: "The Maze Runner",
        author: "James Dashner",
        price: 7.65
    },
    {
        id: 5,
        title: "Macbeth",
        author: "Billy Shakes",
        price: 6.5
    }, {
        id: 6,
        title: "Triumph",
        author: "Jeremey Schaap",
        price: 11.45
    }
];

for (var i = 0; i <= books.length; i++) {
    console.log(books[i]);
}

//creat an array to store users cart info
var cart = [];

//add items to cart -- wired to <a> tags in the HTML
function addToCart(id) {
    //if they have not added any of the titles yet, set the qty property to 1 and add the book to out array
    //otherwise add 1 to the qty
    var bookObj = books[id - 1];
    if (typeof bookObj.qty === 'undefined') { // === means it check data type and value ex "1" === 1 IS FALSE  "1" === "1" is true
        bookObj.qty = 1;
        cart.push(bookObj);
    }
    else {
        bookObj.qty = bookObj.qty + 1;
    }


    /* for testing purposses*/
    //console.clear();
    //for (var i = 0; i < cart.length; i++) {
    //    console.log(cart[i]);

    //}

    //making the notification counter visible in the html
    document.getElementById('cart-notification').style.display = "block";

    //creating the variable to stor the total number of books
    var cartQty = 0;

    //looping through each itme in the cartand summing all of the .qty properties
    for (var i = 0; i < cart.length; i++) {

        cartQty += cart[i].qty;
    }

    //using that running total cariable to displaynumber of book sint he cart notification element
    document.getElementById('cart-notification').innerHTML = cartQty;

    //when setting the innHTML propertyfor the dialog box, we are calling a function that returns a string to display the title information
    document.getElementById('cart-contents').innerHTML = getCartContents();


}
function getCartContents() {
    //creating an empty string to hold info for each title which will be returned at the end of this function
    var cartContent = "";
    //we are creating a running total variable to sum price of all books
    var cartTotal = 0;
    //we are going to show the user which book they have added to the cart, the qty, price per book, and total prive of all books so we need to loop through each item
    //in the cart to display that info
    for (var i = 0; i < cart.length; i++) {
        cartContent += cart[i].title + "<br>by" + cart[i].author + "<br>Qty: " + cart[i].qty + " at " + cart[i].price + "ea.<br><br>"
        cartTotal += cart[i].price * cart[i].qty;

    }
    //at the very end of the string that we a re returning adding the total price ofr all books in the cart
    cartTotal += "Cart Total: $" + cartTotal.toFixed(2);
    //sending this string back to the addtocart function and it is being assigned as the .innerHTMl for the dialog box
    return cartContent;
}
