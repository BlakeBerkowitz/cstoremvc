﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace cStoreMVC.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        #region Welcome to the Global.asax

        /*
        there are times when a developer may want to have an activity ovvur at the application level, regardless of what page(view) is being rendered

        there are also specific points in the application liife cycle that we as developers want certain things to happen. this file provides us the ability
         to run code during specific points in the applications executtion by providing access to a wide range of application life cycle event methods


            Application_Start() this method is invoked when the application first starts and the application domain is created.

        Session _Start() -- this method is invoked each time a new session begins. 
        A session begins when a request is issued from a device or browser for
        the first time, and is often used to initialize user-specific information. 
        ie. banking session about to end; that's the end to session start
        Session timeout defaults to 20 minutes or browser closure. The 20 min 
        mark can be changed in the web.config (root).

        Session_End() -- this method is invoked whenever the user's session ends.
        A session ends when either code explicitly releases the session, the browswer
        is closed, or has not received a request in the stated TimeOut period
        (default 20 minutes)

        Application_BeginRequest() & Application_EndRequest() -- these methods are
        used in conjunction with a multi-threaded application.

        Application_End() -- this method is invoked JUST BEFORE the application ends
        due to specific termination of the application, or explicit shutdown of IIS. 
        This method does NOT run if the server suddenly turns off (power failure)


        */





        #endregion



        protected void Application_Start()
        {

            //declare the application variable to be used in the begin request
            Application["totalUsers"] = 0;
            Application["pageCount"] = 0;

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        


//must be this name
        public void Application_Error()
        {

            //in addition to showing the user a custom error page we should be logging the error then discarding from the server
            //hold the exception in a variable

            Exception ex = Server.GetLastError();
            //get the last error and clear it
            Server.GetLastError();
            Server.ClearError();

            Response.Redirect("/error.html");
        }

        //this is an eventhandler (still a method), standard eventhandler signature is: protected void PAge_Event(object, EventArgs)
        protected void Application_BeginRequest(object sender, EventArgs e)
        {

            //we can manipulate ApplicationLEvel Variables this is available to all users, because of this we need to care for concurrent requests
            // the cariable can only be accessed by 1 user at a time
            //for this reason before we make changes, we lock the application variable then make changes then unlock for others to use

            Application.Lock();
            Application["totalUsers"] = (int)Application["totalUsers"] + 1;
            Application.UnLock();
            //see the footer
        }



    }
}
