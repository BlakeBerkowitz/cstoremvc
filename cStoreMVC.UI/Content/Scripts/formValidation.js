﻿function validateForm(){
    //we want to require each field from the user 
    //get access to each input field
    var name = document.forms['main-contact-form']['name'].value;
    var email = document.forms['main-contact-form']['email'].value;
    var subject = document.forms['main-contact-form']['subject'].value;
    var message = document.forms['main-contact-form']['message'].value;

    //declare a regular expression fro a email
    var emailRegExp = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/);
    //mini lab
    var nameRegExp = new RegExp(/^[a-zA-Z]+(([\'\,\.\-][a-zA-Z])?[a-zA-Z]*)*$/);
    //get error message <span> tags
    var nameVal = document.getElementById('nameVal');
    var emailVal = document.getElementById('emailVal')
    var subjectVal = document.getElementById('subjectVal')
    var messageVal = document.getElementById('messageVal')

    //test all of our conditions later we will check for valid email
    if (name.length == 0 || email.length == 0 || subject.length == 0 || message.length == 0 || !emailRegExp.test(email) || !nameRegExp.test(name)) {
        if (name.length == 0) {
            nameVal.textContent = "*name is required";
        }
        else {
            nameVal.textContent = "";
        }
        if (email.length == 0) {
            emailVal.textContent = "*email is required";
        }
        else {
            emailVal.textContent = "";
        }
        if (subject.length == 0) {
            subjectVal.textContent = "*subject is required";
        }
        else {
            subjectVal.textContent = "";
        }
        if (message.length == 0) {
            messageVal.textContent = "message is required";
        }
        else {
            messageVal.textContent = "";
        }
        if (!emailRegExp.test(email) && email.length > 0) {
            emailVal.textContent = "*Must be in proper form";
        }
        if (emailRegExp.test(email) && email.length > 0) {
            emailVal.textContent = "";
        }
        if (!nameRegExp.test(name) && name.length > 0) {
            nameVal.textContent = "*must be in proper form"
        }
        if (nameRegExp.test(name) && name.length > 0) {
            nameVal.textContent = "";

        }
        //if they fail our criteria this stops the form from submitting
        event.preventDefault();
    }

}