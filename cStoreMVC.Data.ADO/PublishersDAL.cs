﻿using cStore.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cStoreMVC.Data.ADO
{
    public class PublishersDAL
    {
        public string GetPublishersName()
        {
            string publisherName = "";

            SqlConnection conn = new SqlConnection();

            conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security=true;";

            conn.Open();

            SqlCommand cmdGetPublisher = new SqlCommand("select * from Publishers", conn);

            SqlDataReader rdrPublisher = cmdGetPublisher.ExecuteReader();

            while (rdrPublisher.Read())
            {
                publisherName += (string)rdrPublisher["PublisherName"] + ", ";
            }
            rdrPublisher.Close(); //this gets ri of the reader object
            conn.Close();
            return publisherName;
        }

        public List<PublisherModel> GetPublishers()
        {
            //create object to return
            //create a LIst<AuthorModel> to hold the authors
            List<PublisherModel> publishers = new List<PublisherModel>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security=true";
                //open the connection
                conn.Open();

                //create a SQLCommand object
                //command text = select * from authors
                SqlCommand cmdGetPublishers = new SqlCommand("Select * from Publishers", conn);

                //execute the command
                //.ExecuteReader() - for selecting data from a database
                //.ExecuteNonQuery() - for anything that is not a select
                SqlDataReader rdrPublishers = cmdGetPublishers.ExecuteReader();


                //process the DataReader
                while (rdrPublishers.Read())
                {
                    PublisherModel pub = new PublisherModel()
                    {
                        PublisherID = (int)rdrPublishers["PublisherID"],
                        PublisherName = (string)rdrPublishers["PublisherName"],


                        City = (rdrPublishers["City"] is DBNull) ? "N/A" : (string)rdrPublishers["City"],
                        State = (rdrPublishers["State"] is DBNull) ? "N/A" : (string)rdrPublishers["State"],
                    };
                    //add new auth obj to the authors list
                    publishers.Add(pub);
                }

                //close the connection
                rdrPublishers.Close();
            }

            //return the object'
            return publishers;

        }

    }
}
