﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient; //this is for the sqlclinet classes - sqlconnection, sqlcommand, sql datareader, sql parameter
using cStore.Domain.Models;

namespace cStoreMVC.Data.ADO
{
    public class AuthorsDAL
    {
        public string GetFirstnames()
        {
            string firstNames = "";
            //create a sqlconnection objext using the following settings
            //data source = server name (.\sqlexpress
            //initial catalog = database name (cstore)
            //integrated security = true

            SqlConnection conn = new SqlConnection();

            conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security=true;";


            //open the connection
            conn.Open();

            //create a sqlcommand object
            //command text = select *from authors
            SqlCommand cmdGetAuthors = new SqlCommand("select * from authors", conn);

            //execute the command
            //.executereader() - for selecting data from a database
            //.executenonquery() - for anything that is not a select
            SqlDataReader rdrAuthors = cmdGetAuthors.ExecuteReader();
            //process the datareader
            while (rdrAuthors.Read())
            {
                firstNames += (string)rdrAuthors["firstname"] + ", ";
            }
            //close the connection
            rdrAuthors.Close(); //this gets ri of the reader object
            conn.Close();

            return firstNames;
        }

        public List<AuthorModel> GetAuthors()
        {
            //create object to return
            //create a LIst<AuthorModel> to hold the authors
            List<AuthorModel> authors = new List<AuthorModel>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security=true";
                //open the connection
                conn.Open();

                //create a SQLCommand object
                //command text = select * from authors
                SqlCommand cmdGetAuthors = new SqlCommand("Select * from Authors", conn);

                //execute the command
                //.ExecuteReader() - for selecting data from a database
                //.ExecuteNonQuery() - for anything that is not a select
                SqlDataReader rdrAuthors = cmdGetAuthors.ExecuteReader();


                //process the DataReader
                while (rdrAuthors.Read())
                {
                    AuthorModel auth = new AuthorModel()
                    {
                        AuthorID = (int)rdrAuthors["AuthorID"],
                        FirstName = (string)rdrAuthors["FirstName"],
                        LastName = (string)rdrAuthors["LastName"],


                        City = (rdrAuthors["City"] is DBNull) ? "N/A" : (string)rdrAuthors["City"],
                        State = (rdrAuthors["State"] is DBNull) ? "N/A" : (string)rdrAuthors["State"],
                        ZipCode = (rdrAuthors["ZipCode"] is DBNull) ? "N/A" : (string)rdrAuthors["ZipCode"],
                        Country = (rdrAuthors["Country"] is DBNull) ? "N/A" : (string)rdrAuthors["Country"]
                    };
                    //add new auth obj to the authors list
                    authors.Add(auth);
                }

                //close the connection
                rdrAuthors.Close();
            }

            //return the object'
            return authors;

        }

        public void CreateAuthor(AuthorModel author)
        {

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security=true;";
                //create a sqlconnection objext using the following settings
                //data source = server name (.\sqlexpress
                //initial catalog = database name (cstore)
                //integrated security = true

                //open the connection
                conn.Open();

                //create a sqlcommand object
                //command text = select *from authors
                SqlCommand cmdInsertAuthors = new SqlCommand(
                    @"insert into authors (FirstName, LastName, City, State, ZipCode, Country) Values(@FirstName, @LastName, @City, @State, @ZipCode, @Country)", conn);
                // set sql params

                cmdInsertAuthors.Parameters.AddWithValue("FirstName", author.FirstName);
                cmdInsertAuthors.Parameters.AddWithValue("LastName", author.LastName);

                if (author.City != null)
                { cmdInsertAuthors.Parameters.AddWithValue("City", author.City); }
                else
                { cmdInsertAuthors.Parameters.AddWithValue("City", DBNull.Value); }

                if (author.State != null)
                { cmdInsertAuthors.Parameters.AddWithValue("State", author.State); }
                else
                { cmdInsertAuthors.Parameters.AddWithValue("State", DBNull.Value); }

                if (author.ZipCode != null)
                { cmdInsertAuthors.Parameters.AddWithValue("ZipCode", author.ZipCode); }
                else
                { cmdInsertAuthors.Parameters.AddWithValue("ZipCode", DBNull.Value); }

                if (author.ZipCode != null)
                { cmdInsertAuthors.Parameters.AddWithValue("Country", author.Country); }
                else
                { cmdInsertAuthors.Parameters.AddWithValue("Country", DBNull.Value); }


                //execute the command
                //.executereader() - for selecting data from a database
                //.executenonquery() - for anything that is not a select
                cmdInsertAuthors.ExecuteNonQuery();

                //process the datareader

                //close the connection
                conn.Close();

            }
        }

        public AuthorModel GetAuthor(int id)
        {
            AuthorModel author = null;
            using (SqlConnection conn = new SqlConnection())
            {
                //create a sqlconnection objext using the following settings
                //data source = server name (.\sqlexpress
                //initial catalog = database name (cstore)
                //integrated security = true

                conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security=true;";


                //open the connection
                conn.Open();

                //create a sqlcommand object
                //command text = select *from authors
                SqlCommand cmdGetAuthor = new SqlCommand(
                    "select * from authors where AuthorID = @AuthorID", conn);
                cmdGetAuthor.Parameters.AddWithValue("AuthorID", id);
                //execute the command
                //.executereader() - for selecting data from a database
                //.executenonquery() - for anything that is not a select
                SqlDataReader rdr = cmdGetAuthor.ExecuteReader();
                //process the datareader
                while (rdr.Read())
                {
                    author = new AuthorModel()
                    {
                        AuthorID = (int)rdr["AuthorID"],
                        FirstName = (string)rdr["FirstName"],
                        LastName = (string)rdr["LastName"],

                        //deal with nullable fields
                        City = (rdr["City"] is DBNull) ? "" : (string)rdr["City"],
                        State = (rdr["State"] is DBNull) ? "" : (string)rdr["State"],
                        Country = (rdr["Country"] is DBNull) ? "" : (string)rdr["Country"],
                        ZipCode = (rdr["ZipCode"] is DBNull) ? "" : (string)rdr["Zipcode"]
                    };
                }
                //close the connection
                rdr.Close();
            }// end using

            return author;
        }

        public void Updateauthor(AuthorModel author)
        {

            using (
            //create a sqlconnection objext using the following settings
            //data source = server name (.\sqlexpress
            //initial catalog = database name (cstore)
            //integrated security = true

            SqlConnection conn = new SqlConnection())
            {

                conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security=true;";


                //open the connection
                conn.Open();

                //create a sqlcommand object
                //command text = select *from authors
                SqlCommand cmdUpdateAuthor = new SqlCommand(@"Update authors set FirstName = @FirstName, LastName = @LastName, City =@City, State = @State, Country = @country, ZipCode =@ZipCode where AuthorID = @ID", conn);
                cmdUpdateAuthor.Parameters.AddWithValue("ID", author.AuthorID);
                cmdUpdateAuthor.Parameters.AddWithValue("FirstName", author.FirstName);
                cmdUpdateAuthor.Parameters.AddWithValue("LastName", author.LastName);

                if (author.City != null)
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("City", author.City);

                }
                else
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("City", DBNull.Value);

                }

                if (author.State != null)
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("State", author.State);

                }
                else
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("State", DBNull.Value);

                }

                if (author.Country != null)
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("Country", author.Country);

                }
                else
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("Country", DBNull.Value);

                }

                if (author.ZipCode != null)
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("ZipCode", author.ZipCode);

                }
                else
                {
                    cmdUpdateAuthor.Parameters.AddWithValue("ZipCode", DBNull.Value);

                }







                //execute the command
                //.executereader() - for selecting data from a database
                //.executenonquery() - for anything that is not a select
                cmdUpdateAuthor.ExecuteNonQuery();

                //process the datareader
                //close the connection
                conn.Close();
            }//usinng
        }

        public void DeleteAuthor(int id)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                //create a sqlconnection objext using the following settings
                //data source = server name (.\sqlexpress
                //initial catalog = database name (cstore)
                //integrated security = true

                

                conn.ConnectionString = @"Data Source=.\sqlexpress;Initial Catalog=cStore;Integrated Security=true;";


                //open the connection
                conn.Open();

                //create a sqlcommand object
                //command text = select *from authors
                SqlCommand cmdDeleteAuthors = new SqlCommand("delete from authors where AuthorID =@AuthorID", conn);

                //execute the command
                cmdDeleteAuthors.Parameters.AddWithValue("AuthorID", id);
                //.executereader() - for selecting data from a database
                //.executenonquery() - for anything that is not a select
                cmdDeleteAuthors.ExecuteNonQuery();

                //process the datareader
                //close the connection



            }//using
        }
    }
}
