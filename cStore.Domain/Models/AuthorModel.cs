﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cStore.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace cStore.Domain.Models 
{
    public class AuthorModel
    {
        //public int AuthorID { get; set; }

        public int AuthorID { get; set; }

        [Required(ErrorMessage ="required")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }


        [Required(ErrorMessage = "required")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }


        public string City { get; set; }

        [StringLength(2, ErrorMessage ="use two letter abrev. please.")]
        [MinLength(2, ErrorMessage = "use two letter abrev. please.")]

        public string State { get; set; }

        [RegularExpression(@"[0-9] {5}", ErrorMessage = "enter a real zip fool")]
        public string ZipCode { get; set; }

        [Display(Name = "First name")]
        public string Country { get; set; }


    }
}
