﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cStore.Domain.Models;

namespace cStore.Domain.Models
{
    public class PublisherModel
    {
        public int PublisherID { get; set; }
        public string PublisherName { get; set; }
        public string City { get; set; }
        public string State { get; set; }

    }
}
